# In this file we implement Monte-Carlo learning
from statistics import mean

from breakout.game import Game
from breakout.MonteCarloControlBreakout import MonteCarloControlBreakout

from breakout.main import play_game_using_mcc
from breakout.util import read_levels_data_from_csv

if __name__ == "__main__":
    # save_levels_to_csv()
    GRID_DIM_X, GRID_DIM_Y, bricks = read_levels_data_from_csv('game_layouts/large_20x14_v1.csv')
    game = Game(GRID_DIM_X, GRID_DIM_Y, bricks, reset_means_death_policy=True)
    monte_carlo = MonteCarloControlBreakout(epsilon = 0.15, gamma = 0.5)

    #play_game_using_mcc(game, monte_carlo, game_update_time=10)

    print("Running MCC!")
    rewards, trajectories = monte_carlo.run(num_episodes = 5000, game = game)
    print(rewards)

    #print("Run using Dist")
    #play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=False)
    #play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=False)
    #play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=False)
    #play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=False)
    print("Taking best")
    play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=True, initial_ball_speex_x = -2)
    play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=True, initial_ball_speex_x = -1)
    play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=True, initial_ball_speex_x = 0)
    play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=True, initial_ball_speex_x = 1)
    play_game_using_mcc(game, monte_carlo, game_update_time=50, always_take_best_action=True, initial_ball_speex_x = 2)






