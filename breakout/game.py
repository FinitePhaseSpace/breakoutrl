import math
from random import choices

import numpy as np
from numpy import int8

from breakout.entity import Brick, Ball, Paddle


class GameState:
    """
    Represents a GameState. Equals and Hashcode are overwritten so that different object instances that
    represent the same GameState are considered to be equal.
    """

    def __init__(self, board: str, ball_velocity_x: int, ball_velocity_y: int, paddle_speed_x: int):
        self.board = board
        self.ball_velocity_x = ball_velocity_x
        self.ball_velocity_y = ball_velocity_y
        self.paddle_speed_x = paddle_speed_x

    def __eq__(self, other):
        if not isinstance(other, GameState):
            return False
        return self.board == other.board \
            and self.ball_velocity_x == other.ball_velocity_x \
            and self.ball_velocity_y == other.ball_velocity_y \
            and self.paddle_speed_x == other.paddle_speed_x

    def __hash__(self):
        return hash((self.board, self.ball_velocity_x, self.ball_velocity_y, self.paddle_speed_x))


class StateActionPair:
    """
    Represents an ActionTaken under a specific GameState. Equals and Hashcode are overwritten so that different
    object instances that represent the same StateActionPair are considered to be equal.
    """

    def __init__(self, game_state: GameState, action):
        self.game_state = game_state
        self.action = action

    def __eq__(self, other):
        if not isinstance(other, StateActionPair):
            return False
        return self.game_state == other.game_state and self.action == other.action

    def __hash__(self):
        return hash((self.game_state, self.action))


class BreakoutPolicy:
    """
    Maps a game state to a set assigning probabilities to all possible actions (this is done lazily).
    For breakout this consists of 3 possible actions: do nothing, accelerate paddle right, accelerate paddle left.
    The default initialisation for those 3 action possibilities is (1/3, 1/3, 1/3).
    """

    def __init__(self):
        self.policy = {}

    def get_probabilities(self, game_state: GameState):
        """
        Lazily get or initialise and get the probability list for a given state
        :param game_state:
        :return: an existing probability list or a new default initialised instance
        """
        probabilities = self.policy.get(game_state)
        if probabilities is None:
            probabilities = [1 / 3, 1 / 3, 1 / 3]
            self.policy[game_state] = probabilities
        return probabilities

    def update_policy(self, game_state: GameState, probabilities):
        """
        Update the possibility list mapped to a given state
        :param game_state: a GameState
        :param probabilities: the new possibility list
        """
        self.policy[game_state] = probabilities


class Game:
    """
    Holds the state of a game of breakout, provides methods to play a game and returns data for MCC.
    """

    # Possible actions a player can take (nothing, accelerate paddle right, accelerate paddle left)
    possible_actions = (0, 1, -1)

    def __init__(self, grid_dim_x, grid_dim_y, brick_positions, reset_means_death_policy=False, reward_offset=1000):
        self.reset_means_death_policy = reset_means_death_policy
        self.reward_offset = reward_offset
        self.grid_dim_x = grid_dim_x
        self.grid_dim_y = grid_dim_y
        self.brick_positions = brick_positions

        self.paddle = Paddle(math.floor((grid_dim_x - 1) / 2) - 2, grid_dim_y - 1)
        self.bricks = []

        # setup ball and bricks
        self.reset_game()
        self.game_over = False

    def run_using_mcc_policy(self, policy: BreakoutPolicy) -> (list[StateActionPair], int, (np.ndarray, np.ndarray)):
        """
        Run the game until it is finished using a given policy.
        :param: policy: Policy to use for finding moves under a given game state.
        :return: a list of StateActionPairs taken during the game, the reward for finishing thegame, a tuple
        of x,y ball positions observed during gameplay.
        """
        state_action_pair_list = []
        trajectory_x, trajectory_y = [], []
        game_iterations = 0
        while not self.game_over:
            state_action_pair = self.take_action_and_update_game_state(policy)
            state_action_pair_list.append(state_action_pair)
            game_iterations += 1
            trajectory_x.append(self.ball.x)
            trajectory_y.append(self.ball.y)
            if self.reset_means_death_policy and self.reset:  # He died
                return state_action_pair_list, -100, (np.array(trajectory_x), np.array(trajectory_y))
        trajectory_x = np.array(trajectory_x)
        trajectory_y = np.array(trajectory_y)
        return state_action_pair_list, self.get_reward(game_iterations), (trajectory_x, trajectory_y)

    def run_for_evaluation(self, policy: BreakoutPolicy, initial_ball_speex_x=False) -> (
            list[StateActionPair], int, (np.ndarray, np.ndarray)):
        """
        :param policy: policiy used to find actions to take in a given game state
        :param initial_ball_speex_x: (optional) set a desired starting ball speed,
        useful for evaluation of a learned policy.
        :return: observed, ordered StateActionPairs, a tuple of x,y ball position arrays
        """
        self.reset_game()
        if isinstance(initial_ball_speex_x, int):
            self.ball.speed_x = initial_ball_speex_x
        ever_died = False
        state_action_pair_list = []
        trajectory_x, trajectory_y = [], []
        game_iterations = 0
        while (not self.game_over) and len(trajectory_x)<1000:
            state_action_pair = self.take_action_and_update_game_state(policy, take_best=True)
            state_action_pair_list.append(state_action_pair)
            game_iterations += 1
            trajectory_x.append(self.ball.x)
            trajectory_y.append(self.ball.y)
            if self.reset or len(trajectory_x)>=1000:
                ever_died = True
            if self.reset_means_death_policy and self.reset:  # He died
                return state_action_pair_list, -100, (np.array(trajectory_x), np.array(trajectory_y)), ever_died
        trajectory_x = np.array(trajectory_x)
        trajectory_y = np.array(trajectory_y)
        return state_action_pair_list, self.get_reward(game_iterations), (trajectory_x, trajectory_y), ever_died

    def take_action_and_update_game_state(self, policy, take_best=False):
        """
        Take an action and update the game state. The action taken is derived using the provided policy
        :param policy: the policy used to find a new action to take
        :param take_best: only take the best action supplied by the policy
        :return: the state action pair used before the game was updated
        """

        state = self.get_game_state()
        prob = policy.get_probabilities(state)

        action = None
        if take_best:
            for idx, p in enumerate(prob):
                if p > 0.8:
                    action = self.possible_actions[idx]
        if action is None:
            action = choices(Game.possible_actions, weights=prob, k=1)[0]
        if action == -1 or action == 1:
            self.change_Paddle_speed(action)
        self.update_game_state()
        return StateActionPair(state, action)

    def update_game_state(self):
        """
        Update the game by moving the ball to a new position while taking into account collisions.
        """

        # find out where the ball wants to move to on the grid
        new_ball_x = self.ball.x + self.ball.speed_x
        new_ball_y = self.ball.y + self.ball.speed_y

        # Check for collisions
        # rectangle col

        # -------------------------------- NEEDS TO BE UPDATED (same for paddle) the ball can pass through bricks! At
        # the moment a collision is only detected if the ball lands directly on a rectangle tile
        hit_brick = False
        for brick in self.bricks:
            if brick.check_ball_collision(self.getBall().x, self.getBall().y, new_ball_x, new_ball_y):
                hit_brick = True
                self.getBricks().remove(brick)
                self.getBall().speed_y *= -1
                new_ball_x, new_ball_y = self.ball.x, self.ball.y
                break

        if len(self.bricks) == 0:
            self.game_over = True
            return

        # wall col
        self.reset = False
        if not hit_brick:
            if new_ball_y >= self.grid_dim_y:
                # game over --> reset bricks and ball
                self.ball.speed_y *= -1
                new_ball_y = self.grid_dim_y - 1
                self.reset_game()
                self.reset = True
            elif new_ball_y < 0:
                # hit top wall
                self.ball.speed_y *= -1
                new_ball_y = 0
                new_ball_x = self.ball.x
            elif new_ball_x < 0:
                # hit lef wall
                self.ball.speed_x *= -1
                new_ball_x = 0
                new_ball_y = self.ball.y
            elif new_ball_x >= self.grid_dim_x:
                # hit right wall
                self.ball.speed_x *= -1
                new_ball_x = self.grid_dim_x - 1
                new_ball_y = self.ball.y
                pass
            # paddle col
            elif new_ball_y == self.paddle.y:
                distance_from_the_center_of_paddle = (self.ball.x - (self.paddle.x + 2))
                if abs(distance_from_the_center_of_paddle) <= 2:
                    self.ball.speed_y = -1
                    self.ball.speed_x = + distance_from_the_center_of_paddle
                    new_ball_y = self.ball.y
                    new_ball_x = self.ball.x
                else:
                    pass  # We should put a gameover here

        if not self.reset:
            self.ball.x = new_ball_x
            self.ball.y = new_ball_y

        # update paddle position (check that it does not go out of bounds! or hit the ball
        self.paddle.update()
        # paddle collision with walls
        if self.getPaddle().x < 0:
            self.getPaddle().x = 0
            self.getPaddle().speed_x = 0
        elif self.getPaddle().x > self.grid_dim_x - 5:
            self.getPaddle().x = self.grid_dim_x - 5
            self.getPaddle().speed_x = 0

    def reset_game(self):
        """
        Resets: game_over flag, ball/paddle positions/speed. All bricks are regenerated
        :return:
        """
        self.game_over = False
        self.ball = Ball(self.grid_dim_x // 2, self.grid_dim_y - 2)
        self.getPaddle().x = self.grid_dim_x // 2 - 2
        self.getPaddle().speed_x = 0
        self.bricks = []
        for pos in self.brick_positions:
            self.bricks.append(Brick(pos[0], pos[1]))
            # add check all rectangle positions are valid!
            # but we are placing them manually, no? how should we check validity?

    def getBall(self):
        return self.ball

    def getBricks(self):
        return self.bricks

    def getPaddle(self):
        return self.paddle

    def change_Paddle_speed(self, direction):
        self.getPaddle().speed_x += direction
        if abs(self.getPaddle().speed_x) > 2:
            self.getPaddle().speed_x = math.copysign(2, self.getPaddle().speed_x)

    def get_game_state(self):
        """
        Return a compact, comparable form of the current game state
        """
        state = np.zeros((self.grid_dim_x, self.grid_dim_y), dtype=int8)
        try:
            state[self.ball.x, self.ball.y] = 1
            state[self.paddle.x:self.paddle.x + 5, self.paddle.y] = 2
            for brick in self.bricks:
                state[brick.x:brick.x + 3, brick.y] = 3
        except:
            pass
        # print(state.flatten())
        return GameState(''.join(str(x) for x in state.flatten()), self.ball.speed_x, self.ball.speed_y,
                         self.getPaddle().speed_x)

    def get_reward(self, iterations):
        return self.reward_offset + iterations * -1
