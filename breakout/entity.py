# origin of entity is always the left most point!
from random import randint


class Entity:
    """
    Base class of all game entities
    """

    def __init__(self, x, y, dim_x, dim_y):
        self.x = x
        self.y = y
        self.dim_x = dim_x
        self.dim_y = dim_y


class Brick(Entity):
    def __init__(self, x, y):
        super().__init__(x, y, 3, 1)

    def check_ball_collision(self, ball_origin_x: int, ball_origin_y: int, ball_target_x: int, ball_target_y: int):
        """
        Check for collisions with self and a ball
        :return: True iff the ball hits the brick
        """
        return ball_target_y == self.y and ball_target_x >= self.x and ball_target_x < (self.x + self.dim_x)


class Ball(Entity):
    def __init__(self, x, y):
        super().__init__(x, y, 1, 1)
        self.speed_x = randint(-2, 2)
        self.speed_y = -1

    def update(self):
        """
        Update the position of the ball using its current speed
        """
        self.x += self.speed_x
        self.y += self.speed_y


class Paddle(Entity):

    def __init__(self, x, y):
        super().__init__(x, y, 5, 1)
        self.speed_x = 0

    def update(self):
        """
         Update the position of the Paddle using its current speed
         """
        self.x += self.speed_x
