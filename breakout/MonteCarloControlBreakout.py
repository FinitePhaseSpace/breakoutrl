import random
from statistics import mean

import numpy as np

from breakout.game import Game, BreakoutPolicy, GameState, StateActionPair


# possible actions: paddle speed: -1, 0, 1
class MonteCarloControlBreakout:

    def __init__(self, epsilon, gamma):
        # Policy: maps stats to probabilities for all possible actions
        self.policy = BreakoutPolicy()
        # Value Function: maps a state/action tuple to a real Value
        self.value_function = {}

        # Map state/action pairs to rewards
        self.rewards = {}

        self.epsilon = epsilon
        self.prob_other = self.epsilon / 3
        self.prob_best = 1 - self.epsilon + self.epsilon / 3
        self.gamma = gamma
        pass

    def run(self, num_episodes, game):
        """
        Run the MCC algorithm for a requested number of episodes on a given game instance
        :param num_episodes: number of episodes to play
        :param game: game instance to play
        :return: rewards observed per episode, ball trajectories
        """
        rewards = []
        trajectories = []
        for i in range(0, num_episodes):
            game.reset_game()
            print("MCC: Running Episode: " + str(i) + "/" + str(num_episodes), end="\r")
            episode_reward, episode_trajectory = self.__run_episode(game)
            rewards.append(episode_reward)
            trajectories.append(episode_trajectory)
        print("MCC: finished!")
        return rewards, trajectories

    def __run_episode(self, game):
        """
        Run the mcc algorithm for an episode
        :param game: game instance to use
        :return: reward, ball trajectory
        """
        # Generate Episode
        game_episode = game.run_using_mcc_policy(self.policy)
        state_action_pair_list = game_episode[0]
        reward = game_episode[1]
        trajectory = game_episode[2]
        state_action_pair_list.reverse()
        # or return reversed list!

        expected_return = reward
        for t in range(0, len(state_action_pair_list)):
            expected_return = self.gamma * expected_return

            # check if state_action_tuple of timestamp t is present in a state_action_tuple from 0 .. t-1
            state_action_tuple = state_action_pair_list[t]
            if state_action_tuple not in state_action_pair_list[0:t - 1]:
                self.process_state_action_pair(expected_return, state_action_tuple)
        return reward, trajectory

    def process_state_action_pair(self, expected_return, state_action_tuple):
        """
        Update the reward array, value function and policy for a given game state, action and reward
        :param expected_return: expected reward
        :param state_action_tuple: State of the game and action taken
        """
        reward_array = self.update_and_get_rewards(state_action_tuple, expected_return)
        self.value_function[state_action_tuple] = mean(reward_array)
        best_action = self.get_best_action(state_action_tuple.game_state)
        updated_probabilities = [self.prob_other, self.prob_other, self.prob_other]
        updated_probabilities[best_action] = self.prob_best
        self.policy.update_policy(state_action_tuple.game_state, updated_probabilities)

    # return reward list and init to empty list if not present
    def get_reward_array(self, state_action_pair: StateActionPair):
        reward = self.rewards.get(state_action_pair)
        if reward is None:
            reward = []
            self.rewards[state_action_pair] = reward
        return reward

    def update_and_get_rewards(self, state_action_pair: StateActionPair, new_reward):
        """
        Append a new reward to the reward list for a given state action pair and return the updated list
        :param state_action_pair: a state action pair
        :param new_reward: a newly observed reward to append
        :return: the updated reward list
        """
        rewards = self.get_reward_array(state_action_pair)
        rewards.append(new_reward)
        return rewards

    def get_state_action_value(self, state_action_pair: StateActionPair):
        value = self.value_function.get(state_action_pair)
        if value is None:
            value = 0  # arbitrary! TODO look for bugs!
            self.value_function[state_action_pair] = value
        return value

    def get_best_action(self, game_state: GameState):
        """
        Returns the best action that can be taken for a given game state
        """
        a1 = self.get_state_action_value(StateActionPair(game_state, -1))
        a2 = self.get_state_action_value(StateActionPair(game_state, 0))
        a3 = self.get_state_action_value(StateActionPair(game_state, 1))
        a_list = [a1, a2, a3]
        return a_list.index(max(a_list)) - 1

    def evaluation(self, game):
        """
        Run a game for all possible starting trajectories and return survived trajectories
        """
        game.reset_game()
        game_episodes = [game.run_for_evaluation(self.policy, initial_ball_speex_x = speed) for speed in [-2,-1,0,1,2]]
        state_action_pair_lists = [row[0] for row in game_episodes]
        rewards = [row[1] for row in game_episodes]
        trajectories = [row[2] for row in game_episodes]
        ever_died_all = [row[3] for row in game_episodes]
        trajectories_survived = []
        for i,ever_died in enumerate(ever_died_all):
            if ever_died:
                trajectories_survived.append(([],[]))
            else:
                trajectories_survived.append(trajectories[i])
        return trajectories_survived







