import math
import sys

import numpy as np
import pygame
import time
from breakout.MonteCarloControlBreakout import MonteCarloControlBreakout

from breakout.game import Game
from breakout.util import read_levels_data_from_csv

from PIL import Image, ImageOps



# from breakout.pygame_recorder import ScreenRecorder

COLOUR_BLACK = (0, 0, 0)
COLOUR_WHITE = (200, 200, 200)
COLOUR_IDK = (123, 5, 202)
COLOUR_HMM = (222, 100, 222)

BLOCKSIZE = 20  # Set the size of the grid cell

GRID_DIM_X = 15
GRID_DIM_Y = 20
WINDOW_HEIGHT = GRID_DIM_X * BLOCKSIZE
WINDOW_WIDTH = GRID_DIM_Y * BLOCKSIZE

GAME_UPDATE_TIME = 100



def main():
    """
    Call this function to play a game of Breakout
    """
    # # GRID_DIM_X, GRID_DIM_Y, brick_defs = read_levels_data_from_csv('game_layouts/simple_layout.csv')
    # GRID_DIM_X, GRID_DIM_Y, brick_defs = read_levels_data_from_csv('game_layouts/medium_15x9_block.csv')
    GRID_DIM_X, GRID_DIM_Y, brick_defs = read_levels_data_from_csv('game_layouts/rectangle/small_15x9_10_bricks.csv')

    # REFACTOR!
    WINDOW_HEIGHT = GRID_DIM_X * BLOCKSIZE
    WINDOW_WIDTH = GRID_DIM_Y * BLOCKSIZE

    global SCREEN, CLOCK
    pygame.init()
    SCREEN = pygame.display.set_mode((WINDOW_HEIGHT, WINDOW_WIDTH))
    CLOCK = pygame.time.Clock()
    SCREEN.fill(COLOUR_BLACK)

    # ADD IFELSE STATEMENT SO USER CAN CHOOSE WHICH LAYOUT TO USE

    # random placement of bricks
    # game = Game(GRID_DIM_X, GRID_DIM_Y, [(3, 3), (3, 7), (6, 5), (12, 5), (11, 8)])

    # parallel lines
    # game = Game(GRID_DIM_X, GRID_DIM_Y, [(0, 3), (3, 3), (6, 5), (9, 7), (12, 7)])

    # rectangle
    # game = Game(GRID_DIM_X, GRID_DIM_Y, [(0, 3), (3, 3), (6, 3), (9, 3), (12, 3)])

    # pyramid
    # game = Game(GRID_DIM_X, GRID_DIM_Y, [(0, 5), (3, 4), (6, 3), (9, 4), (12, 5)])

    # simple game

    game = Game(GRID_DIM_X, GRID_DIM_Y, brick_defs)

    delta_time = 0
    while True:
        delta_time += CLOCK.tick(60)

        if delta_time >= GAME_UPDATE_TIME:
            # do a game tick
            game.update_game_state()
            delta_time = 0
            if game.game_over:
                return

        # GRAPHICS are always drawn, do not move into game update loop!
        SCREEN.fill(COLOUR_BLACK)
        drawGrid()
        drawBall(game.getBall())
        drawBricks(game.getBricks())
        drawPaddle(game.getPaddle())

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    game.change_Paddle_speed(-1)
                if event.key == pygame.K_RIGHT:
                    game.change_Paddle_speed(1)

        pygame.display.update()


def play_game_using_mcc(game: Game, mcc: MonteCarloControlBreakout, game_update_time=20, blocksize=20, always_take_best_action = True, initial_ball_speex_x = False, out_file='output.avi'):
    """
    Renders and executes a game using a MonteCarloControlBreakout instance.
    :param game: game to play
    :param mcc: MonteCarloControlBreakout instance
    :param game_update_time: milliseconds to between two game ticks
    :param blocksize: size of a block in pixels
    :param always_take_best_action: force the policy to always return the best action (no exploration)
    :param initial_ball_speex_x: overwrite the starting ball trajectory
    """
    game.reset_game()
    if isinstance(initial_ball_speex_x, int):
        game.ball.speed_x = initial_ball_speex_x

    global SCREEN, CLOCK
    pygame.init()
    WINDOW_HEIGHT = game.grid_dim_x * blocksize
    WINDOW_WIDTH = game.grid_dim_y * blocksize
    SCREEN = pygame.display.set_mode((WINDOW_HEIGHT, WINDOW_WIDTH))
    CLOCK = pygame.time.Clock()
    SCREEN.fill(COLOUR_BLACK)
    CLOCK = pygame.time.Clock()

    frames = []
    # FPS = 30
    # recorder = ScreenRecorder(WINDOW_WIDTH, WINDOW_HEIGHT, FPS, out_file = out_file)

    iterations = 0
    delta_time = 0
    while True:
        delta_time += CLOCK.tick(60)

        if delta_time >= game_update_time:
            # do a game tick
            game.take_action_and_update_game_state(mcc.policy, always_take_best_action)
            delta_time = 0
            iterations = iterations + 1
            if game.game_over: #or iterations > 50:
                pygame.quit()
                return

        # GRAPHICS are always drawn, do not move into game update loop!
        SCREEN.fill(COLOUR_BLACK)
        drawGrid()
        drawBall(game.getBall())
        drawBricks(game.getBricks())
        drawPaddle(game.getPaddle())

        pygame.display.update()

        frames.append( ImageOps.flip(Image.fromarray(pygame.surfarray.pixels3d(SCREEN))) )
        # recorder.capture_frame(SCREEN)
    # recorder.end_recording()
        frames[0].save(out_file, save_all=True, append_images=frames[1:], duration=50, loop=0)


def drawGrid():
    """
    Draw the game grid
    """
    for x in range(GRID_DIM_X):
        for y in range(GRID_DIM_Y):
            rect = pygame.Rect(x * BLOCKSIZE, y * BLOCKSIZE,
                               BLOCKSIZE, BLOCKSIZE)
            pygame.draw.rect(SCREEN, COLOUR_WHITE, rect, 1)


def drawBall(ball):
    pygame.draw.circle(SCREEN, COLOUR_IDK, ((ball.x + 1 / 2) * BLOCKSIZE, (ball.y + 1 / 2) * BLOCKSIZE), BLOCKSIZE / 2)


def drawBricks(bricks):
    for brick in bricks:
        rect = pygame.Rect(brick.x * BLOCKSIZE, brick.y * BLOCKSIZE,
                           BLOCKSIZE * brick.dim_x, BLOCKSIZE * brick.dim_y)
        pygame.draw.rect(SCREEN, COLOUR_HMM, rect)


def drawPaddle(paddle):
    rect = pygame.Rect(paddle.x * BLOCKSIZE, paddle.y * BLOCKSIZE, BLOCKSIZE * paddle.dim_x, BLOCKSIZE * paddle.dim_y)
    pygame.draw.rect(SCREEN, COLOUR_WHITE, rect)
    pass


if __name__ == "__main__":
    main()
