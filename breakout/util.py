import csv


def save_levels_to_csv():
    f = open('game_layouts/medium_15x9_v1.csv', 'w')
    writer = csv.writer(f)
    header = ['GRID_DIM_X', 'GRID_DIM_Y', 'Bricks']
    data = [15,20, 0, 3, 3, 3, 6, 5, 9, 7, 12, 7]
    writer.writerow(header)
    writer.writerow(data)

def read_levels_data_from_csv(file_name):
    f = open(file_name, 'r')
    reader = csv.reader(f)
    level_data = []
    for row in reader:
        level_data.append(row)
    GRID_DIM_X, GRID_DIM_Y = level_data[1][0:2]
    GRID_DIM_X, GRID_DIM_Y = int(GRID_DIM_X), int(GRID_DIM_Y)
    bricks = []
    # for i in range(2, len(level_data[1]), 2):
    #     bricks.append((int(level_data[1][i]), int(level_data[1][i + 1])))
    #----a.astype(np.int)
    for row in level_data[3::]:
        row_int = [int(row[0]),int(row[1])]
        bricks.append(row_int)
    return GRID_DIM_X, GRID_DIM_Y, bricks