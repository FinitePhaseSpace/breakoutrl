# BreakoutRL

To play the game install all required dependencies and run main.py.

Use run_mcc.py to quickly try out training a MCC algorithm on a Game of Breakout.

game.py#run_using_mcc_policy() allows running a game by using a policy.

main.py#play_game_using_mcc() allows running and rendering a game using a policy