import unittest
from breakout.MonteCarloControlBreakout import *


def get_MCC() -> MonteCarloControlBreakout:
    return MonteCarloControlBreakout(0.1, 0.95)
class MonteCarloControlBreakoutTest(unittest.TestCase):

    def test_get_reward_array(self):
        mcc = get_MCC()

        sap = StateActionPair(GameState("01", 0, 0, 1), 0)

        rewards = mcc.get_reward_array(sap)
        self.assertTrue(np.array_equal(rewards, []))

        rewards_updated = mcc.update_and_get_rewards(sap, 1)
        self.assertIs(rewards, rewards_updated)
        self.assertTrue(np.array_equal(rewards_updated, [1]))

        same_sate = StateActionPair(GameState("01", 0, 0, 1), 0)
        rewards_same_state = mcc.get_reward_array(same_sate)
        self.assertIs(rewards, rewards_same_state)

        rewards_same_state_updated = mcc.update_and_get_rewards(same_sate, 0)
        self.assertTrue(np.array_equal(rewards_same_state_updated, [1, 0]))



def test_run():
    pass


def test_process_state_action_pair():
    pass


def test_get_state_action_value():
    pass


def test_get_best_action():
    pass

if __name__ == '__main__':
    unittest.main()






