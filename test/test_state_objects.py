import unittest

import numpy as np

from breakout.game import GameState
from breakout.game import StateActionPair


class TestStateObjects(unittest.TestCase):
    def test_game_state_equality(self):
        a = GameState("01", 0, 0, -1)
        b = 3
        self.assertNotEqual(a, b)

        a = GameState("01", 0, 0, -1)
        b = GameState("01", 0, 0, -1)
        self.assertEqual(a, b)

        a = GameState("01", 0, 1, -1)
        b = GameState("01", 0, 0, -1)
        self.assertNotEqual(a, b)

        a = GameState("01", 0, 0, -1)
        b = GameState("00", 0, 0, -1)
        self.assertNotEqual(a, b)

    def test_game_state_hash(self):
        a = GameState("01", 0, 0, -1)
        b = 3
        self.assertNotEqual(hash(a), hash(b))

        a = GameState("01", 0, 0, -1)
        b = GameState("01", 0, 0, -1)
        self.assertEqual(hash(a), hash(b))

        a = GameState("01", 0, 1, -1)
        b = GameState("01", 0, 0, -1)
        self.assertNotEqual(hash(a), hash(b))

        a = GameState("01", 0, 0, -1)
        b = GameState("00", 0, 0, -1)
        self.assertNotEqual(hash(a), hash(b))

    def test_StateActionPair_equality(self):
        gs_1 = GameState("01", 0, 0, -1)
        gs_1_1 = GameState("01", 0, 0, -1)
        gs_2 = GameState("05", 0, 0, -1)

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_1, 1)
        self.assertEqual(a, b)

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_1_1, 1)
        self.assertEqual(a, b)

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_1, 0)
        self.assertNotEqual(a, b)

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_2, 1)
        self.assertNotEqual(a, b)

    def test_StateActionPair_hash(self):
        gs_1 = GameState("01", 0, 0, -1)
        gs_1_1 = GameState("01", 0, 0, -1)
        gs_2 = GameState("05", 0, 0, -1)

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_1, 1)
        self.assertEqual(hash(a), hash(b))

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_1_1, 1)
        self.assertEqual(hash(a), hash(b))

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_1, 0)
        self.assertNotEqual(hash(a), hash(b))

        a = StateActionPair(gs_1, 1)
        b = StateActionPair(gs_2, 1)
        self.assertNotEqual(hash(a), hash(b))


if __name__ == '__main__':
    unittest.main()
